/**********************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación sobre Linux 1/3 */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo    */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional  */
/**********************************************************************/
/* Acceso a una memoria I2C tipo 24C02/24C04/24C08/24C16              */
/* gcc -Wall main.c i2cfunc.c -o i2c                                  */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "i2cfunc.h"
#include <unistd.h>

#define I2CBUS 1
#define I2C_ADDRESS 0x50
#define ADD_TEST1 0x10
#define ADD_TEST2 0x40

//i2cdetect -y -r -a 1    muestra todo lo que hay en el i2c


int at24c16_read(int fd, char reg_addr, unsigned char * data);
int at24c16_write(int fd, unsigned char reg_addr, unsigned char data);

int main(void){
	int fd = i2c_open(I2CBUS, I2C_ADDRESS);
	unsigned char data = 0;
	int i,j;
	char string[10]="SASE-2017";
	char c;

	printf("Test I2C Flash\n");

	if(fd > 0){
		printf("open() ok\n");
      
		c = getchar();getchar();
 		printf("at24c16_read()\n\n");
		at24c16_read(fd, ADD_TEST1, &data);
		printf("0x%02X = 0x%02X '%c'\n\n",ADD_TEST1, data, data);

		printf("at24c16_write('%c')\n\n",c);
		at24c16_write(fd, ADD_TEST1, c);usleep(1000);

		printf("at24c16_read()\n\n");
		at24c16_read(fd, ADD_TEST1, &data);
		printf("0x%02X = 0x%02X '%c'\n\n",ADD_TEST1, data, data);

		
		
		getchar();
		printf("____________________________________________________\n");
		
		//Leo
		for (i=ADD_TEST2,j=0;j<strlen(string);i++,j++) {
			at24c16_read(fd, i, &data);
			printf("0x%02X = 0x%02X '%c'\n",i, data, data);
		}

		//escribo
		for (i=ADD_TEST2,j=0;j<strlen(string);i++,j++,usleep(1000))
			at24c16_write(fd, i, string[j]);

		//vuelvo a leer
		for (i=ADD_TEST2,j=0;j<strlen(string);i++,j++) {
			at24c16_read(fd, i, &data);
			printf("0x%02X = 0x%02X '%c'\n",i, data, data);
		}

		
		printf("____________________________________________________\n");
		printf("\nMapa de memoria\n     ");
		for(i=0;i<16;i++) 
			printf("0x%02X ",i);
		printf("\n");
		for(j=0;j<0x100;j+=0x10){
			printf("0x%02X ",j);
			for(i=0;i<0x10;i++) {
				at24c16_read(fd, 0x100+j+i, &data);
				printf(" '%c' ",data);
			}
			printf("\n");
		}
		
		i2c_close(fd);
   }
   
   return fd;
}

/********************************************************************************/
int at24c16_read(int fd, char reg_addr, unsigned char * data){
	int rv = -1;
	rv = i2c_write_byte(fd, reg_addr);
	if(rv > 0){
		rv = i2c_read_byte(fd, data);
	}
	return rv;
}
/********************************************************************************/
int at24c16_write(int fd, unsigned char reg_addr, unsigned char data){
	unsigned char buf[2] = {reg_addr, data};
	return i2c_write(fd, buf, 2);
}
/********************************************************************************/


