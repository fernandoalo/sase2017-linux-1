/**********************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación sobre Linux 1/3 */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo    */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional  */
/**********************************************************************/
/* Forma muy rudimentaria de implementar un PWM del led.              */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LED "/sys/class/leds/beaglebone:green:usr"

#define MAX_LEDS 4
#define MAX_STRING 64 
#define T_MAX 25
#define STEPS 200



typedef struct {
	int trigger;
	int brightness;
} beaglebone_leds_st;



int main (void) {
	beaglebone_leds_st leds[MAX_LEDS];
	char string[MAX_STRING];
	useconds_t ton=0,t_total=STEPS/2+1;
	int i,j,k,step=1;
	
	printf("Test LEDS\n");
	
	for (i=0;i<MAX_LEDS;i++) {
		sprintf(string,"%s%i/%s",LED,i,"trigger"); 		//creo el string del archivo trigger para cada led
		leds[i].trigger = open(string,O_WRONLY);   		// abro todos los trigger
		write(leds[i].trigger,"none", strlen("none")); 	// cambio el tipo de trigger a "none"
		close(leds[i].trigger); 						// cierro el archivo de trigger ya no lo necesito
		
		sprintf(string,"%s%i/%s",LED,i,"brightness"); // abro todos los brigtness
		leds[i].brightness = open(string,O_WRONLY);   // abro el archivo brightness para cada led
	}

	for (k=0;k<20;k++,step=1) {
		for (j=0;j<STEPS;j++,ton+=step) {
			for (i=0;i<MAX_LEDS;i++) 
				write(leds[i].brightness,"1", strlen("1")); 
			usleep(ton*100);
			for (i=0;i<MAX_LEDS;i++) 
				write(leds[i].brightness,"0", strlen("0")); 
			usleep((t_total-ton)*100);
			if (j==STEPS/2) step=-1;
		}
	}
	
	for (i=0;i<MAX_LEDS;i++) 
		close(leds[i].brightness); //cierro los archivos de brillo
	
	
	return 0;
}
	