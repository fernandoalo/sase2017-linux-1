/**********************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación sobre Linux 1/3 */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo    */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional  */
/**********************************************************************/
/* Utilización de un GPIO                                             */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define PATH_GPIO "/sys/class/gpio"
#define PATH_GPIO60 "/sys/class/gpio/gpio60"

#define MAX_STRING 64

int main (void) {
	char string[MAX_STRING];
	int fd_aux,gpio=0;
	
	printf("Test GPIO60\n");
	
	//prinero declaro que voy a usar el pin como GPIO
	sprintf(string,"%s/%s",PATH_GPIO,"export"); //creo el string del archivo trigger para cada led
	fd_aux = open(string,O_WRONLY); 
	write(fd_aux,"60", strlen("60")); // especifico el GPIO_60 como GPIO
	close(fd_aux);

	//ahora especifico que es una salida
	sprintf(string,"%s/%s",PATH_GPIO60,"direction"); //creo el string del archivo trigger para cada led
	fd_aux = open(string,O_WRONLY); 
	write(fd_aux,"out", strlen("out")); // especifico el GPIO_60 como salida
	close(fd_aux);

	//ya puedo utilizarlo como salida 
	//abro el archivo "value" para poder setear el valor
	sprintf(string,"%s/%s",PATH_GPIO60,"value"); //creo el string del archivo trigger para cada led
	fd_aux = open(string,O_WRONLY); 
	
	
	while(getchar()=='\n') {

		if (gpio) 
			write(fd_aux,"0", strlen("0")); // apago el led
		else 
			write(fd_aux,"1", strlen("1")); // prendo el led

		gpio=~gpio; 						//toggleo el valor de gpio
	}
	
	close(fd_aux);
	
	return 0;
}
	