/**********************************************************************/
/* Ejemplos utilizados en el SASE 2017 - Programación sobre Linux 1/3 */
/* Fernando Aló - fernandoalo.utn@gmail.com - linkd.in/fernandoalo    */
/* Laboratorio de Procesamiento Digital - Departamento de Electrónica */
/* Facultad Regional Buenos Aires / Universidad Tecnológica Nacional  */
/**********************************************************************/

//sudo apt-get install gcc-arm-linux-gnueabi       /* para instalar el compilador    */
//arm-linux-gnueabi-gcc -Wall leds1.c -o leds1     /* compilación del codigo fuente  */
//scp leds1 root@192.168.7.2:~/                    /* copia segura a través de la IP */

//Modos de disparo posibles para los leds
//none nand-disk mmc0 mmc1 timer oneshot [heartbeat] backlight gpio cpu0 default-on transient 

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main (void) {
	int fd_trigger, fd_brightness;
	
	printf("Test LED 1\n");
	
	fd_trigger = open("/sys/class/leds/beaglebone:green:usr0/trigger",O_WRONLY);
	write(fd_trigger,"none", strlen("none"));
	close(fd_trigger);
	
	fd_brightness = open("/sys/class/leds/beaglebone:green:usr0/brightness",O_WRONLY);

	while(1) {
		write(fd_brightness,"1", 1); 
		usleep(250000);
		write(fd_brightness,"0", 1); 
		usleep(250000);
	}
	
	close(fd_brightness);
	return 0;
}
	